from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView  # , DeleteView, UpdateView
from django.urls import reverse_lazy
from .models import Receipt, ExpenseCategory, Account


# Create your views here.


@login_required
def show_receipts_list(request):
    receipts = Receipt.objects.filter(purchaser=request.user).order_by("-date")
    context = {"receipts": receipts}
    return render(request, "receipts/list.html", context)


class ReceiptCreateView(LoginRequiredMixin, CreateView):
    model = Receipt
    template_name = "receipts/create.html"
    fields = ["vendor", "total", "tax", "date", "category", "account"]

    def form_valid(self, form):
        receipt = form.save(commit=False)
        receipt.purchaser = self.request.user
        receipt.save()
        return redirect("home")


@login_required
def list_expense_categories(request):
    expense_categories = ExpenseCategory.objects.all().order_by("name")

    context = {
        "category_list": [],
    }

    for category in expense_categories:
        context["category_list"].append(
            {
                "name": category.name,
                "receipts": len(
                    Receipt.objects.filter(
                        purchaser=request.user, category=category
                    )
                ),
            }
        )

    return render(request, "receipts/categories.html", context)


@login_required
def list_accounts(request):
    accounts = Account.objects.filter(owner=request.user).order_by("name")

    context = {
        "account_list": [],
    }

    for account in accounts:
        context["account_list"].append(
            {
                "name": account.name,
                "number": account.number,
                "receipts": len(
                    Receipt.objects.filter(
                        purchaser=request.user, account=account
                    )
                ),
            }
        )

    return render(request, "receipts/accounts.html", context)


class CategoryCreateView(LoginRequiredMixin, CreateView):
    model = ExpenseCategory
    template_name = "receipts/create_category.html"
    fields = ["name"]

    def form_valid(self, form):
        category = form.save(commit=False)
        category.owner = self.request.user
        category.save()
        return redirect("expense_categories_list")


class AccountCreateView(LoginRequiredMixin, CreateView):
    model = Account
    template_name = "receipts/create_account.html"
    fields = ["name", "number"]

    def form_valid(self, form):
        account = form.save(commit=False)
        account.owner = self.request.user
        account.save()
        return redirect("accounts_list")
