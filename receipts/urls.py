from django.urls import path
from receipts.views import (
    show_receipts_list,
    ReceiptCreateView,
    list_expense_categories,
    list_accounts,
    CategoryCreateView,
    AccountCreateView,
)

urlpatterns = [
    path(
        "",
        show_receipts_list,
        name="home",
    ),
    path(
        "create/",
        ReceiptCreateView.as_view(),
        name="create_receipt",
    ),
    path(
        "categories/",
        list_expense_categories,
        name="expense_categories_list",
    ),
    path(
        "categories/create/",
        CategoryCreateView.as_view(),
        name="create_category",
    ),
    path(
        "accounts/",
        list_accounts,
        name="accounts_list",
    ),
    path(
        "accounts/create/",
        AccountCreateView.as_view(),
        name="create_account",
    ),
]
