from django.shortcuts import render, redirect
from django.contrib.auth import login
from django.contrib.auth.forms import UserCreationForm

# Create your views here.


def user_create_view(request):
    if request.method == "GET":
        form = UserCreationForm
        return render(request, "registration/signup.html", {"form": form})
    elif request.method == "POST":
        form = UserCreationForm(request.POST)
        if form.is_valid():
            user = form.save()
            if user is not None:
                login(request, user)
                return redirect("home")
        else:
            return render(request, "registration/signup.html", {"form": form})
